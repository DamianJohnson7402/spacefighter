
#include "Projectile.h"

Texture* Projectile::s_pTexture = nullptr;

Projectile::Projectile()
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(-Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

//Detailed Documentation Two
//This method is used to keep position of the Projectile Object Updated on it's location and state. It will run determined on current game time.
void Projectile::Update(const GameTime* pGameTime)
{
	//Checks for if the Bullet is currently active, and will run the following code if the bullet object is active.
	if (IsActive())
	{
		//References Vector2 class in the Vector2 header file to determine what speed and direction the bullet object should move in.
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed();
		//Executes the Translation defined above onto the bullet object for it to execute that movement.
		TranslatePosition(translation);

		//References Vector2 class in the Vector2 header file to track the current position of the bullet object.
		Vector2 position = GetPosition();

		//References Vector2 class in the Vector2 header file to track the current size of the object's texture, and draws that onto the object.
		Vector2 size = s_pTexture->GetSize();

		// Is the projectile off the screen? -- Section checks if object is off the screen
		//Checks if the bullet object has left the play area via the Bottom Border. Will deactivate bullet object if true.
		if (position.Y < -size.Y) Deactivate();
		//Checks if the bullet object has left the play area via the Left Border. Will deactivate bullet object if true.
		else if (position.X < -size.X) Deactivate();
		//Checks if bullet object has left the play area via the Top Border. Will deactivate bullet object if true.
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();
		//Checks if bullet object has left the play area via the Right Border. Will deactivate bullet object if true.
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();
	}

	//Executes all the changes to the Object that were executed, and updates that info to the object.
	GameObject::Update(pGameTime);
}

void Projectile::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

void Projectile::Activate(const Vector2& position, bool wasShotByPlayer)
{
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);

	GameObject::Activate();
}

std::string Projectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType Projectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}