
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}

//used to keep track and update status of enemyship object
void EnemyShip::Update(const GameTime *pGameTime)
{
	//this checks if m_delayedseconds is greater than 0.
	if (m_delaySeconds > 0)
	{
		//Updates time elapsed by using m_delaySecond's original value, and subtracting that against how much time has passed. Will count down to zero.
		m_delaySeconds -= pGameTime->GetTimeElapsed();

		if (m_delaySeconds <= 0)
		{
			//Activates the GameObject with a Activate function.
			GameObject::Activate();
		}
	}

	//This if-statement will run if the instance of the EnemyShip object is currently enabled.
	if (IsActive())
	{
		//Tracks how long the object has been activated for by adding elapsedTime onto the m_activationSeconds.
		m_activationSeconds += pGameTime->GetTimeElapsed();
		//Deactivation of the object occurs when m_activationSeconds is above a value of 2, and object is not on screen.
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate();
	}
	//Takes new data and executes an update statement to apply that data to the Ship object, replacing the old data.
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}